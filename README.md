# Whiteframe

Extremely short (shorter than its `flake.nix`) script that opens its argument in a separate window with
web view. Supposed to be used from scripts, not as an application.

Allows to decouple things with web rendering (e.g. Markdown previewers) from browser.
