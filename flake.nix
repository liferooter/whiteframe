{
  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {inherit system;};
    in {
      packages = rec {
        whiteframe = pkgs.stdenv.mkDerivation {
          pname = "whiteframe";
          version = "1.0";

          src = self;

          nativeBuildInputs = with pkgs; [
            buildPackages.glib
            buildPackages.gtk4
            wrapGAppsHook4
          ];

          buildInputs = with pkgs; [
            gtk4 webkitgtk_6_0
            gobject-introspection glib-networking
            (python3.withPackages (ps: with ps; [
              pygobject3
            ]))
          ];

          installPhase = ''
            mkdir -p $out/bin
            mv main.py $out/bin/whiteframe
          '';
        };
        default = whiteframe;
      };
    });
}
