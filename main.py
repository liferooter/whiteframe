#!/usr/bin/env python3

import gi

gi.require_versions({'Gtk': '4.0', 'WebKit': '6.0'})

from gi.repository import Gtk, WebKit, Gio, GLib

from sys import argv

URI = argv[1]

def on_activate(app: Gtk.Application):
    view = WebKit.WebView(hexpand=True, vexpand=True)
    GLib.idle_add(lambda: view.load_uri(URI))

    shortcut_controller = Gtk.ShortcutController.new()
    shortcut_controller.add_shortcut(Gtk.Shortcut.new(
        Gtk.ShortcutTrigger.parse_string('<Control>R'),
        Gtk.CallbackAction.new(lambda *args: view.reload())
    ))

    view.add_controller(shortcut_controller)

    win = Gtk.ApplicationWindow.new(app)
    win.props.decorated = False
    win.set_child(view)

    win.present()


if __name__ == '__main__':
    app = Gtk.Application.new(
        'io.gitlab.liferooter.WhiteFrame',
        Gio.ApplicationFlags.NON_UNIQUE
    )

    app.connect('activate', on_activate)
    app.run()
